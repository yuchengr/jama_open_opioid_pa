Adams County	223	8	195	20	0
Allegheny County	19638	843	16986	1430	379
Armstrong County	133	4	123	6	0
Beaver County	715	35	659	20	1
Bedford County	50	2	43	5	0
Berks County	1640	75	1350	161	54
Blair County	333	17	263	53	0
Bradford County	70	3	63	4	0
Bucks County	1572	61	1305	200	6
Butler County	360	18	296	45	1
Cambria County	395	23	319	47	6
Cameron County	8	2	4	2	0
Carbon County	143	15	123	5	0
Centre County	1087	48	941	98	0
Chester County	2276	121	1953	192	10
Clarion County	128	6	117	5	0
Clearfield County	100	2	93	4	1
Clinton County	125	3	112	9	1
Columbia County	303	15	258	29	1
Crawford County	199	12	172	15	0
Cumberland County	735	34	642	58	1
Dauphin County	2217	88	1785	330	14
Delaware County	2046	119	1793	130	4
Elk County	36	2	32	2	0
Erie County	1894	79	1702	109	4
Fayette County	491	31	423	37	0
Forest County	2	0	2	0	0
Franklin County	376	26	295	49	6
Fulton County	16	1	14	1	0
Greene County	78	5	67	6	0
Huntingdon County	65	3	61	1	0
Indiana County	351	30	293	28	0
Jefferson County	76	5	65	6	0
Juniata County	25	1	20	4	0
Lackawanna County	1242	48	1073	93	28
Lancaster County	1656	64	1480	98	14
Lawrence County	282	20	244	18	0
Lebanon County	167	5	145	15	2
Lehigh County	1407	52	1218	105	32
Luzerne County	766	34	684	36	12
Lycoming County	321	18	247	56	0
McKean County	46	5	38	3	0
Mercer County	618	41	546	28	3
Mifflin County	33	3	28	2	0
Monroe County	663	23	593	42	5
Montgomery County	2545	112	2145	269	19
Montour County	29	1	25	3	0
Northampton County	1578	78	1387	83	30
Northumberland County	121	10	100	11	0
Perry County	60	2	57	1	0
Philadelphia County	36335	1536	32770	1764	265
Pike County	52	4	46	1	1
Potter County	8	0	5	3	0
Schuylkill County	362	8	335	15	4
Snyder County	55	1	48	6	0
Somerset County	151	6	121	24	0
Sullivan County	6	0	4	2	0
Susquehanna County	18	0	18	0	0
Tioga County	42	3	32	7	0
Union County	81	8	71	2	0
Venango County	415	6	208	6	195
Warren County	37	7	27	3	0
Washington County	812	34	719	56	3
Wayne County	75	7	55	12	1
Westmoreland County	993	60	853	75	5
Wyoming County	19	2	16	1	0
York County	1620	88	1370	153	9