'''
***************************************************************************************************
This script accompanies the following paper:

Sarker A, Gonzalez-Hernandez G, Ruan Y, Perrone J.
Machine Learning and Natural Language Processing for Geolocation-Centric Monitoring and
Characterization of Opioid-Related Social Media Chatter.
JAMA Netw Open. 2019 Nov 1;2(11):e1914672. doi: 10.1001/jamanetworkopen.2019.14672.

The script was modified from several online examples. Please follow the parameter values specified
in the paper to obtain comparable results.

For questions, please contact:
Abeed Sarker (abeed@dbmi.emory.edu)
Yucheng Ruan (ycruan@seas.upenn.edu)

'''

import pandas as pd

# read the unlabeled tweets in the data frame
unlabeled_raw = pd.read_csv('./LabeledData/unlabeled_tweets_filtered.txt', header=None, delimiter='\t', dtype=object)
# drop the NaN columns and rename the rest columns
unlabeled_cleaned = unlabeled_raw.drop([4, 8], axis=1)

unlabeled_cleaned.columns = ['tweet_id', 'user_id', 'tweet', 'timestamp', 'language', 'county', 'city']
# change 'tweet_id' column to int type
unlabeled_cleaned['tweet_id'] = unlabeled_cleaned['tweet_id'].astype(int)

# read all the prediction results in data frame
prediction = pd.read_csv('predictions_on_filtered_unlabeled_data_svm.txt', header=None, delimiter='\t', names=['tweet_id', 'label_predicted'])

# merger all the entries based on the tweet id
labeled = unlabeled_cleaned.merge(prediction, left_on='tweet_id', right_on='tweet_id', how='inner')

# filter out unwanted data

# labeled_1 = labeled[labeled['label_predicted'] == 1]
labeled_1 = labeled

# change the timestamp to date type
labeled_time = labeled_1.copy()
labeled_time.loc[:, 'timestamp'] = pd.to_datetime(labeled_1['timestamp'])

# check if there is null value in county column
print labeled_time[labeled_time['county'].isnull()]

# delete the null county rows
labeled_copy = labeled_time.copy()
labeled_copy = labeled_copy.dropna(subset=['county'])
labeled_copy = labeled_copy.reset_index(drop=True)

# divide the data in 2012, 2013, 2014, 2015
results_2012 = labeled_copy[labeled_copy['timestamp'].between('2012', '2013')]
results_2013 = labeled_copy[labeled_copy['timestamp'].between('2013', '2014')]
results_2014 = labeled_copy[labeled_copy['timestamp'].between('2014', '2015')]
results_2015 = labeled_copy[labeled_copy['timestamp'].between('2015', '2016')]

# compute the total number of tweets per county per year
total_2012 = pd.DataFrame(results_2012.groupby(['county']).count()['tweet_id']).rename(
    columns={'tweet_id': 'count'}).reset_index()
total_2013 = pd.DataFrame(results_2013.groupby(['county']).count()['tweet_id']).rename(
    columns={'tweet_id': 'count'}).reset_index()
total_2014 = pd.DataFrame(results_2014.groupby(['county']).count()['tweet_id']).rename(
    columns={'tweet_id': 'count'}).reset_index()
total_2015 = pd.DataFrame(results_2015.groupby(['county']).count()['tweet_id']).rename(
    columns={'tweet_id': 'count'}).reset_index()

# compute the total number of tweets per county per year on label 1
label_1_2012 = pd.DataFrame(
    results_2012[results_2012['label_predicted'] == 1].groupby(['county']).count()['tweet_id']).rename(
    columns={'tweet_id': 'count_label_1'}).reset_index()
label_1_2013 = pd.DataFrame(
    results_2013[results_2013['label_predicted'] == 1].groupby(['county']).count()['tweet_id']).rename(
    columns={'tweet_id': 'count_label_1'}).reset_index()
label_1_2014 = pd.DataFrame(
    results_2014[results_2014['label_predicted'] == 1].groupby(['county']).count()['tweet_id']).rename(
    columns={'tweet_id': 'count_label_1'}).reset_index()
label_1_2015 = pd.DataFrame(
    results_2015[results_2015['label_predicted'] == 1].groupby(['county']).count()['tweet_id']).rename(
    columns={'tweet_id': 'count_label_1'}).reset_index()

# compute the total number of tweets per county per year on label 2
label_2_2012 = pd.DataFrame(
    results_2012[results_2012['label_predicted'] == 2].groupby(['county']).count()['tweet_id']).rename(
    columns={'tweet_id': 'count_label_2'}).reset_index()
label_2_2013 = pd.DataFrame(
    results_2013[results_2013['label_predicted'] == 2].groupby(['county']).count()['tweet_id']).rename(
    columns={'tweet_id': 'count_label_2'}).reset_index()
label_2_2014 = pd.DataFrame(
    results_2014[results_2014['label_predicted'] == 2].groupby(['county']).count()['tweet_id']).rename(
    columns={'tweet_id': 'count_label_2'}).reset_index()
label_2_2015 = pd.DataFrame(
    results_2015[results_2015['label_predicted'] == 2].groupby(['county']).count()['tweet_id']).rename(
    columns={'tweet_id': 'count_label_2'}).reset_index()

# compute the total number of tweets per county per year on label 3
label_3_2012 = pd.DataFrame(
    results_2012[results_2012['label_predicted'] == 3].groupby(['county']).count()['tweet_id']).rename(
    columns={'tweet_id': 'count_label_3'}).reset_index()
label_3_2013 = pd.DataFrame(
    results_2013[results_2013['label_predicted'] == 3].groupby(['county']).count()['tweet_id']).rename(
    columns={'tweet_id': 'count_label_3'}).reset_index()
label_3_2014 = pd.DataFrame(
    results_2014[results_2014['label_predicted'] == 3].groupby(['county']).count()['tweet_id']).rename(
    columns={'tweet_id': 'count_label_3'}).reset_index()
label_3_2015 = pd.DataFrame(
    results_2015[results_2015['label_predicted'] == 3].groupby(['county']).count()['tweet_id']).rename(
    columns={'tweet_id': 'count_label_3'}).reset_index()

# compute the total number of tweets per county per year on label 4
label_4_2012 = pd.DataFrame(
    results_2012[results_2012['label_predicted'] == 4].groupby(['county']).count()['tweet_id']).rename(
    columns={'tweet_id': 'count_label_4'}).reset_index()
label_4_2013 = pd.DataFrame(
    results_2013[results_2013['label_predicted'] == 4].groupby(['county']).count()['tweet_id']).rename(
    columns={'tweet_id': 'count_label_4'}).reset_index()
label_4_2014 = pd.DataFrame(
    results_2014[results_2014['label_predicted'] == 4].groupby(['county']).count()['tweet_id']).rename(
    columns={'tweet_id': 'count_label_4'}).reset_index()
label_4_2015 = pd.DataFrame(
    results_2015[results_2015['label_predicted'] == 4].groupby(['county']).count()['tweet_id']).rename(
    columns={'tweet_id': 'count_label_4'}).reset_index()

# merge all the data in 2012
# also fill all the nan value with 0 after left join
data_2012 = total_2012.merge(label_1_2012, left_on=['county'], right_on=['county'], how='left').merge(label_2_2012, left_on=['county'], right_on=['county'], how='left').merge(
    label_3_2012, left_on=['county'], right_on=['county'], how='left').merge(label_4_2012, left_on=['county'], right_on=['county'], how='left').fillna(0).set_index('county')

data_2012 = data_2012.astype(int)
data_2012.to_excel("2012_Pennsylvania.xlsx", engine='xlsxwriter')

# merge all the data in 2013
# also fill all the nan value with 0 after left join
data_2013 = total_2013.merge(label_1_2013, left_on=['county'], right_on=['county'], how='left').merge(label_2_2013, left_on=['county'], right_on=['county'], how='left').merge(
    label_3_2013, left_on=['county'], right_on=['county'], how='left').merge(label_4_2013, left_on=['county'], right_on=['county'], how='left').fillna(0).set_index('county')

data_2013 = data_2013.astype(int)
data_2013.to_excel("2013_Pennsylvania.xlsx", engine='xlsxwriter')

# merge all the data in 2014
# also fill all the nan value with 0 after left join
data_2014 = total_2014.merge(label_1_2014, left_on=['county'], right_on=['county'], how='left').merge(label_2_2014, left_on=['county'], right_on=['county'], how='left').merge(
    label_3_2014, left_on=['county'], right_on=['county'], how='left').merge(label_4_2014, left_on=['county'], right_on=['county'], how='left').fillna(0).set_index('county')

data_2014 = data_2014.astype(int)
data_2014.to_excel("2014_Pennsylvania.xlsx", engine='xlsxwriter')

# merge all the data in 2015
# also fill all the nan value with 0 after left join
data_2015 = total_2015.merge(label_1_2015, left_on=['county'], right_on=['county'], how='left').merge(label_2_2015, left_on=['county'], right_on=['county'], how='left').merge(
    label_3_2015, left_on=['county'], right_on=['county'], how='left').merge(label_4_2015, left_on=['county'], right_on=['county'], how='left').fillna(0).set_index('county')

data_2015 = data_2015.astype(int)
data_2015.to_excel("2015_Pennsylvania.xlsx", engine='xlsxwriter')

