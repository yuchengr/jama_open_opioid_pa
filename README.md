## Introduction

The problem of drug addiction and overdose has rearched epidemic proportions in the United States and current monitoring strategies have a substantial time lag. Our work focus on developing and validating an text-processing pipeline for geospatial and temporal analysis of opioid-mentioning social media.

## Prerequisites

```
pip install tensorflow
pip install sklearn
pip install scipy
pip install plotly
```

## Files

* DNN_classifier.py

* OpioidTweetClassifier.py

* OpioidOverdoseCorrelationCalculator.py

* EnsembleEvaluations.py

* DataCountCalculator.py

* SubstateLevel_Geomaps.py

* TweetAbuse_DeathCount_Heatmaps.py

* Auxiliary

* CorrelationData

* LabeledData

  The data format is `Tweet_id - - Label Text`. For example,

  > 382576381332574208	-	-	3	Not sure it's appropriate to use heroin as a hash tag #areyoustupid

## Paper

Sarker A, Gonzalez-Hernandez G, Ruan Y, Perrone J. Machine learning and natural language processing for geolocation-centric monitoring and characterization of opioid-related Twitter chatter. JAMA Network Open. 2019; 2(11). [doi:10.1001/jamanetworkopen.2019.14672](https://jamanetwork.com/journals/jamanetworkopen/fullarticle/2753983)

